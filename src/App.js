import React, { Component } from 'react';
import './App.css';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom';
import Header from './Header'
import Home from './Home'

class App extends Component {
  render() {
    return (
     <Router>
       <div className="App">
        <Switch>
          <Route path="/checkout"></Route>
          <Route path="/login"></Route>
          <Route path="/">
            <Header/>
            <Home/>
          </Route>
        </Switch>
      </div>
     </Router> 
      
    );
  }
}

export default App;
