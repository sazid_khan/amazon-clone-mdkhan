import React from 'react';
import './Home.css';

function Home() {
    return (
        <div className="home">
            <img
             className="home__image"
             src="https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Fuji/2020/March/Fuji_TallHero_ShipATW_Modified_en_US_1x._CB419795291_.jpg" 
             />
        </div>
    )
}

export default Home;
